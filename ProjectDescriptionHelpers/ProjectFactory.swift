import ProjectDescription
import Foundation

/// Creates `Project` instance, which is directly used for generating the project file.
/// - Parameters:
///   - name: Name of the app.
///   - minimalOSVersion: Minimal iOS version, which should the app support.
///   - devices: Supported devices. Default is `.iphone`
///   - packages: List of all Swift packages, which will the project use.
///   - targetSpecs: List of targets specs.
///   - additionalBuildScripts: Additional scripts to be run during the build (i.e. Build Phases). Default value is `[]`.
///   - additionalTargets: Additional target like frameworks, libraries, notification targets etc. Default value is `[]`.
///   - dependencies: Common dependencies, which should be added to all targets.
///   - testTargets: List of testing targets specs
/// - Returns: Casted `Project` instance.
public func createProject(name: String, minimalOSVersion: String,
                          devices: DeploymentDevice = .iphone, packages: [Package],
                          targetSpecs: [TargetSpec], additionalBuildScripts: [TargetScript] = [],
                          additionalTargets: [Target] = [],
                          dependencies: [TargetDependency],
                          testTargets: [TestTargetSpec] = [],
                          runScripts: [RunScript] = RunScript.allCases) -> Project {
    let targets = targetSpecs.map { $0.getTarget(dependencies: dependencies,
                                                 minOSVersion: minimalOSVersion,
                                                 devices: devices,
                                                 additionalScripts: additionalBuildScripts,
                                                 runScripts: runScripts) }
    + testTargets.map { $0.getTarget(appName: name,
                                     minOSVersion: minimalOSVersion,
                                     devices: devices) }
    + additionalTargets
    let schemes = targetSpecs.map { targetSpec -> Scheme in
        let testTargetsForCurrentTarget = testTargets.filter { $0.target == targetSpec.name }
        let testTargetsNames = testTargetsForCurrentTarget.map { $0.name }
        let testTargetsPlans = testTargetsForCurrentTarget.compactMap { $0.testPlans }.flatMap { $0 }
        return targetSpec.getScheme(
            testTargets: testTargetsNames.isEmpty ? nil : testTargetsNames,
            testPlans: testTargetsPlans
        )
    }

    for script in runScripts {
        for path in script.targetScript.outputPaths {
            var path = path.pathString
            if path.contains("$(SRCROOT)") {
                path = path.replacingOccurrences(of: "$(SRCROOT)", with: ".")
            }

            var rootPath = URL(fileURLWithPath: #file)
                .deletingLastPathComponent()
                .deletingLastPathComponent()
                .deletingLastPathComponent()

            rootPath.appendPathComponent(path)
            var filePath = rootPath.relativePath

            if !FileManager.default.fileExists(atPath: filePath) {
                print("Creating empty file for path \(filePath)")
                try? FileManager.default.createDirectory(at: rootPath.deletingLastPathComponent(),
                                                         withIntermediateDirectories: true)
                FileManager.default.createFile(atPath: filePath,
                                               contents: nil)
            }
        }
    }

    return Project(
        name: name,
        organizationName: "MeguMethod",
        packages: packages,
        settings: .settings(base: SettingsDictionary()
            .appleGenericVersioningSystem()),
        targets: targets,
        schemes: schemes,
        additionalFiles: ["Entitlements/**"],
        resourceSynthesizers: [.strings(), .fonts()]
    )
}
