import ProjectDescription

/// Enumeration of possible signing identities.
public enum SigningIdentity: String {

    /// Mainly used for Debug versions
    case developer = "iPhone Developer"
    /// Needs to be used for every version which is distributed to App Store, Test Flight or Firebase App Distribution (or other testing distribution platform)
    case distribution = "iPhone Distribution"
}
