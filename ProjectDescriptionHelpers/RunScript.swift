import ProjectDescription

/// Enumerates all common run scripts, which can be used during the apps build.
public enum RunScript: CaseIterable {

    case autoSpacing
    case swiftLint
    case markOrderPrivateExtension
    case uploadCrashlytics
    case custom(TargetScript)

    public static var allCases: [RunScript] = [
        .autoSpacing,
        .swiftLint,
        .markOrderPrivateExtension,
        .uploadCrashlytics
    ]

    /// Creates Tuist `TargetScript` type for the `RunScript` case.
    public var targetScript: TargetScript {
        switch self {
        case .autoSpacing:
            return TargetScript.pre(
                tool: "python3",
                arguments: ["${PROJECT_DIR}/tuist/Scripts/AutoSpacing.py Sources"],
                name: "Auto spacing",
                basedOnDependencyAnalysis: false,
                runForInstallBuildsOnly: true
            )
        case .swiftLint:
            return TargetScript.pre(
                path: "tuist/Scripts/SwiftLintRunScript.sh",
                name: "SwiftLint",
                basedOnDependencyAnalysis: false
            )
        case .markOrderPrivateExtension:
            return TargetScript.pre(
                tool: "python3",
                arguments: ["${PROJECT_DIR}/tuist/Scripts/MarkOrder+PrivateExtensionLintRunScript.py"],
                name: "Python lints",
                basedOnDependencyAnalysis: false
            )
        case .uploadCrashlytics:
            return TargetScript.post(
                script: "${BUILD_DIR%/Build/*}/SourcePackages/checkouts/firebase-ios-sdk/Crashlytics/run",
                name: "Upload to Crashlytics",
                inputPaths: [
                    "${DWARF_DSYM_FOLDER_PATH}/${DWARF_DSYM_FILE_NAME}/Contents/Resources/DWARF/${TARGET_NAME}",
                    "$(SRCROOT)/$(BUILT_PRODUCTS_DIR)/$(INFOPLIST_PATH)"
                ]
            )
        case .custom(let script):
            return script
        }
    }

    public var shouldRunOnCI: Bool {
        switch self {
        case .uploadCrashlytics, .custom:
            return true
        case .swiftLint, .markOrderPrivateExtension, .autoSpacing:
            return false
        }
    }
}
