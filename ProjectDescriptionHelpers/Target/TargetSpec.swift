import ProjectDescription

/// Spec for target
public struct TargetSpec {

    // MARK: - Properties
    public let name: String
    public let displayName: String
    let bundleId: String
    let appIconId: String?
    let debugSpec: ConfigurationSpec
    let releaseSpec: ConfigurationSpec
    let additionalDependencies: [TargetDependency]
    let additionalSources: [String]
    let additionalResources: [String]
    let coreDataModels: [CoreDataModel]

    // MARK: - Init
    /// Defines target parameters.
    /// - Parameters:
    ///   - name: Name of the target. Is used also as a name for its build scheme.
    ///   - displayName: Final name of the app, which is visible to the user.
    ///   - bundleId: Unique bundle ID for the target.
    ///   - appIconId: Identifer of App icon set in `.xcassets`. If `nil` is passed, the default one is used. Default is `nil`.
    ///   - debugSpec: _Build Settings_ for the _Debug_ version.
    ///   - releaseSpec: _Build Settings_ for the _Release_ version.
    ///   - additionalDependencies: Additional dependencies, which is specific for the target. The common dependencies should be passed at once to the `getTarget` function. Default values is `[]`.
    ///   - additionalSources: Additional sources, which is specific for the target. All common sources should be located in the `Sources` folder (except `R.generated.swift` file, which is located right in the root). Default value is `[]`.
    ///   - additionalResources: Additional resources, which is specific for the target. All common resources should be located in the `Resources` or `Localized` folder. Default value is `[]`.
    public init(name: String, displayName: String, bundleId: String, appIconId: String? = nil,
                debugSpec: ConfigurationSpec, releaseSpec: ConfigurationSpec,
                additionalDependencies: [TargetDependency] = [],
                additionalSources: [String] = [], additionalResources: [String] = [], coreDataModels: [CoreDataModel] = []) {
        self.name = name
        self.displayName = displayName
        self.bundleId = bundleId
        self.appIconId = appIconId
        self.debugSpec = debugSpec
        self.releaseSpec = releaseSpec
        self.additionalDependencies = additionalDependencies
        self.additionalSources = additionalSources
        self.additionalResources = additionalResources
        self.coreDataModels = coreDataModels
    }
}

// MARK: - Cast to Tuist type
public extension TargetSpec {

    /// Casts internal target spec to Tuist `Target` type.
    /// - Parameters:
    ///   - dependencies: All frameworks, libraries and embedded content on which the target should depend.
    ///   - minOSVersion: Minimal iOS version to be set as the deployment target.
    ///   - devices: Supported devices. Default is `.iphone`
    ///   - additionalScripts: Additional scripts to be run during the build (i.e. Build Phases).
    /// - Returns: Casted `Target` instance.
    func getTarget(dependencies: [TargetDependency],
                   minOSVersion: String,
                   devices: DeploymentDevice = .iphone,
                   additionalScripts: [TargetScript],
                   runScripts: [RunScript]) -> Target {
        let isCI = Environment.isCI.getBoolean(default: false)
        let runScripts = isCI ? runScripts.filter { $0.shouldRunOnCI } : runScripts
        let resources = (["Resources/**", "Localized/**"] + additionalResources).map { ResourceFileElement(stringLiteral: $0) }
        return Target(
            name: name,
            platform: .iOS,
            product: .app,
            bundleId: bundleId,
            deploymentTarget: .iOS(targetVersion: minOSVersion, devices: devices),
            infoPlist: "Sources/Info.plist",
            sources: SourceFilesList(globs: ["Sources/**"] + additionalSources),
            resources: ResourceFileElements(resources: resources),
            entitlements: "Entitlements/\(name).entitlements",
            scripts: runScripts.map { $0.targetScript } + additionalScripts,
            dependencies: dependencies + additionalDependencies,
            settings: .settings(
                debug: debugSpec.getConfigurationSettings(targetName: name, displayName: displayName, appIconId: appIconId),
                release: releaseSpec.getConfigurationSettings(targetName: name, displayName: displayName, appIconId: appIconId)
            ),
            coreDataModels: coreDataModels
        )
    }

    /// Creates build scheme for the target.
    /// - Parameter testTargets: If the target is intented to be tested with unit/UI tests, the test targets names needs to be listed here. Default value is `nil`.
    /// - Returns: Casted Tuist `Scheme` type for the created build scheme.
    func getScheme(testTargets: [String]? = nil, testPlans: [Path]? = nil) -> Scheme {
        var testAction: TestAction?

        if let testTargets = testTargets {
            testAction = TestAction.targets(testTargets.map { TestableTarget(stringLiteral: $0) })
        }

        if let testPlans = testPlans {
            testAction = TestAction.testPlans(testPlans)
        }

        return Scheme(
            name: name,
            shared: true,
            buildAction: BuildAction(
                targets: [TargetReference(stringLiteral: name)]
            ),
            testAction: testAction
        )
    }
}
