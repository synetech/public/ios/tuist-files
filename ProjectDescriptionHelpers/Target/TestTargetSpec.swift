import ProjectDescription

/// Spec for testing target
public struct TestTargetSpec {

    // MARK: - Options
    public enum Option {
        case target(bundleId: String)
        case testPlans(_ paths: [String])
    }

    // MARK: - Properties
    let name: String
    let type: TestsType
    let bundleId: String
    let target: String
    let targetProductName: String
    let additionalSources: [String]
    let additionalDependencies: [TargetDependency]
    let additionalCompileFlags: [String]?
    let testPlans: [Path]?
    let coreDataModels: [CoreDataModel]

    // MARK: - Init
    /// Defined parameters for testing target.
    /// - Parameters:
    ///   - name: Name of the target. Has to equal name of folder in which source files for the target is located. Is also used as suffix for the product name.
    ///   - type: Define whether unit or UI tests should be created.
    ///   - bundleId: Unique bundle ID for the target.
    ///   - target: Name of the target which should be tested.
    ///   - targetProductName: Product name of the target which should be tested. Needs to be set right in the build setttings, because the `Target`'s `productName` parameter doesn't allow spaces.
    ///   - additionalSources: Additional sources, which is specific for the target. All common sources should be located in folder named same as the targets name. Default value is `[]`.
    ///   - additionalDependencies: Additional dependencies, which is specific for the target. Common dependency is the testing target, but additional ones can be added (e.g. if the testing target is using some library directly). Default values is `[]`.
    ///   - additionalCompileFlags: Adds custom compile flags to the target. Default value is `nil`.
    ///   - option: Specifies if the tests are a regular target or a set of test plans
    public init(name: String, type: TestsType,
                target: String, targetProductName: String,
                additionalSources: [String] = [], additionalDependencies: [TargetDependency] = [],
                additionalCompileFlags: [String]? = nil,
                option: Option, coreDataModels: [CoreDataModel] = []) {
        self.name = name
        self.type = type
        self.target = target
        self.targetProductName = targetProductName
        self.additionalSources = additionalSources
        self.additionalDependencies = additionalDependencies
        self.additionalCompileFlags = additionalCompileFlags
        switch option {
        case .target(let bundleId):
            self.bundleId = bundleId
            self.testPlans = nil
        case .testPlans(let testPlans):
            self.bundleId = ""
            self.testPlans = testPlans.compactMap { Path($0) }
        }
        self.coreDataModels = coreDataModels
    }

    // MARK: - Type
    /// Enumerates both types of the automatic tests.
    public enum TestsType {

        case unit
        case ui

        var product: Product {
            switch self {
            case .unit:
                return .unitTests
            case .ui:
                return .uiTests
            }
        }
    }
}

// MARK: - Cast to Tuist type
public extension TestTargetSpec {

    /// Casts internal target spec to Tuist `Target` type.
    /// - Parameters:
    ///   - appName: Name of the whole app. Used as prefix for product name of the target.
    ///   - minOSVersion: Minimal version of the OS.
    ///   - devices: Supported devices. Default is `.iphone`
    /// - Returns: Casted `Target` instance.
    func getTarget(appName: String, minOSVersion: String, devices: DeploymentDevice = .iphone) -> Target {
        var settings = SettingsDictionary()
        if let additionalCompileFlags = additionalCompileFlags {
            settings[BuildSetting.compilationConditions.rawValue] = .array([BuildSettingsConstant.debugCompileFlag.rawValue] + additionalCompileFlags)
        }
        if type == .unit {
            settings[BuildSetting.testHost.rawValue] = .string("$(BUILT_PRODUCTS_DIR)/\(targetProductName).app/\(targetProductName)")
        }
        settings[BuildSetting.developmentTeam.rawValue] = .string("RA935TF4A8")
        return Target(
            name: name,
            platform: .iOS,
            product: type.product,
            productName: appName + name,
            bundleId: bundleId,
            deploymentTarget: .iOS(targetVersion: minOSVersion, devices: devices),
            infoPlist: testPlans == nil ? InfoPlist(stringLiteral: "\(name)/Info.plist") : .default,
            sources: SourceFilesList(globs: ["\(name)/**"] + additionalSources),
            dependencies: [
                .target(name: target)
            ] + additionalDependencies,
            settings: .settings(
                base: settings
            ),
            coreDataModels: coreDataModels
        )
    }
}
